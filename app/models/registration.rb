class Registration < ApplicationRecord
    
    # has_many :type_movements
    belongs_to :concept
    belongs_to :movement
    belongs_to :user
end
