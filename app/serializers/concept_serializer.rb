class ConceptSerializer < ApplicationSerializer
  attributes :id, :name

  has_many :registrations
end
