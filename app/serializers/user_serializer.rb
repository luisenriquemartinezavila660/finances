class UserSerializer < ApplicationSerializer
  attributes :id, :name, :email, :age, :ocupation, :phone, :balance

  has_many :registrations
end
