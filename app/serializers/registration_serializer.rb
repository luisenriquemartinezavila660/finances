class RegistrationSerializer < ApplicationSerializer
  attributes :id, :amount, :date, :description

  belongs_to :user
  belongs_to :concept
  belongs_to :movement
end
