module Api
    module V1
        class ConceptsController < ApiBaseController 
            skip_forgery_protection
            def index 
                concepts = Concept.all()
                render json: concepts
            end
            def save_type_concept
                begin
                    concept = Concept.new(permited_params)
                    if concept.save!
                        render json:{message:'se guardo con exito',data: concept}
                    end
                rescue => exception
                    render json:{message:'ocurrio un error', exception: exception}
                end
            end

            def update_type_concept
                
                concept = Concept.find(params[:id])
                concept.name = params[:name]
                if concept.save!
                    render json:{message:'Actualizado con exito', data: concept}
                else
                    render json: {message:'ocurrio un error'}
                end

            end

            def delete_type_concept
                concept = Concept.find(params[:id])
                if concept.destroy!
                    render json:{message:'eliminado con exito'}
                else
                    render json:{message:'error al eliminar'}
                end
            end
            private
            def permited_params
                params.permit(:name)
            end
        end
    end
end
