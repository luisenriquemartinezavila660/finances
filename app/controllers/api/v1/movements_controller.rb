module Api
    module V1
        class MovementsController < ApiBaseController
            skip_forgery_protection
            def index 
                movements = Movement.all()
                render json: movements
            end
            def save_type_movements
                begin
                    movement = Movement.new(permited_params)
                    if movement.save!
                        render json:{message:'se guardo con exito',data: movement}
                    end
                rescue => exception
                    render json:{message:'ocurrio un error', exception: exception}
                end
            end

            def update_type_movement
                
                movement = Movement.find(params[:id])
                movement.name = params[:name]
                if movement.save!
                    render json:{message:'Actualizado con exito', data: movement}
                else
                    render json: {message:'ocurrio un error'}
                end

            end

            def delete_type_movement
                movement = Movement.find(params[:id])
                if movement.destroy!
                    render json:{message:'eliminado con exito'}
                else
                    render json:{message:'error al eliminar'}
                end
            end
            private
            def permited_params
                params.permit(:name)
            end
        end
    end
end
