module Api
    module V1
        class RegistrationsController < ApiBaseController
            skip_forgery_protection
            def index
                #byebug
                registrations = Registration.where(user_id: @current_user.id)
                render json: registrations
            end
            def get_balance
                balance = @current_user.balance
                render json:{balance: balance}
            end
            def save_registration
                #byebug
                registration = Registration.new
                registration.user_id = @current_user.id
                registration.movement_id = params[:movement_id]
                registration.amount =params[:amount]
                registration.concept_id =params[:concept_id]
                registration.date = params[:date]
                registration.description =params[:description]
                if registration.save! && operation_balance(params[:movement_id],params[:amount])
                    render json:{message: 'registro guardado con exito'}
                else
                    render json:{message:'problemas al guardar'}
                end
            end
            def add_balance_user
                @current_user.balance += params[:balance]
                @current_user.save!
                render json: {message:"Se añadio el monto con exito", balance: @current_user.balance}
            end
            private
            
            def operation_balance(movement,amount)
                if movement == 1
                    @current_user.balance -= amount
                    @current_user.save 
                else
                    @current_user.balance += amount
                    @current_user.save
                end
            end

            def permited_params
                params.permit(:user_id,:movement_id,:amount,:concept_id,:date,:description)
            end
        end
    end
end
