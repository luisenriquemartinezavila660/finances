module Api
    module V1
        class ApiBaseController < ApplicationController
            before_action :authenticate
            # rescue_from ::Exception, :with => :rescue_exception
            # rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
            # rescue_from ActionController::RoutingError, :with => :routing_error
            include ActionController::HttpAuthentication::Token::ControllerMethods
            #include DeviseTokenAuth::Concerns::SetUserByToken
            
            def undefined_route
                
            routing_error
            end
            def authenticate
                
                authenticate_token || user_signed_in? || render_unauthorized
            end
            def authenticate_token
                authenticate_with_http_token do |token, _options|
                    @current_user = User.find_by(api_key: token)
                end
            end

            def render_unauthorized(realm = 'Application')
                headers['WWW-Authenticate'] = %(Token realm="#{realm.delete('"')}")
                render json: { errors: 'Bad credentials' }, status: :unauthorized
            end

            def render_not_found_response
                render json: { message: 'No existe el recurso solicitado.',
                            code: 'not_found' },
                    status: :not_found
            end
            
            protected
            def render_unknown_error(error)
                render json: { status: 'error', message: 'Error en la aplicación', error: error }, status: :internal_server_error
            end

            def allow_access
            return true
            end
            
            def deny_access
            render :json => {
                :success  =>  false,
                :message  =>  "Access denied"
            }.to_json
                
            return false
            end
            
            def routing_error(exception = nil)
            # deliver_exception_notification(exception) if exception
            
            render :json => {
                :success => false,
                :message => "Invalid/Undefined API"
            }.to_json
            end
            
            def record_not_found(exception)
            # deliver_exception_notification(exception)
            
            render :json => {
                :success => false,
                :message => "Record not found"
            }.to_json
            end
            
            def rescue_exception(exception)
            # deliver_exception_notification(exception)
            
            render :json => {
                :success => false,
                :message => "Exception occured",
                :exception => exception.inspect
            }.to_json
            end
            def encode_token(payload)
                JWT.encode(payload, 'yourSecret')
            end

            def auth_header
                # { Authorization: 'Bearer <token>' }
                request.headers['Authorization']
            end

            def decoded_token
                if auth_header
                    token = auth_header.split(' ')[1]
                    # header: { 'Authorization': 'Bearer <token>' }
                    begin
                        JWT.decode(token, 'yourSecret', true, algorithm: 'HS256')
                    rescue JWT::DecodeError
                        nil
                    end
                end
            end

            def logged_in_user
                if decoded_token
                user_id = decoded_token[0]['user_id']
                @user = User.find_by(id: user_id)
                end
            end

            def logged_in?
                !!logged_in_user
            end

            def authorized
                render json: { message: 'Please log in' }, status: :unauthorized unless logged_in?
            end
        end
    end
end