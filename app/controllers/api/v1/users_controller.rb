module Api
    module V1
        class UsersController < ApiBaseController
            #before_action :authorized, except: [:login, :create]
            skip_before_action :verify_authenticity_token
            skip_before_action :authenticate
            # REGISTER
            def create
                
                @user = User.create(user_params)
                if @user.valid?
                    @user.api_key =  JWT.encode({id: @user.id, exp: 1.month.from_now.to_i}, Rails.application.secrets.secret_key_base)
                    @user.save!
                    render json: {user: @user, token: @user.api_key}
                else
                    render json: {error: "Invalid email or password"}
                end
            end
        
            # LOGGING IN
            def login
                @user = User.find_by_email(params[:email])
            
                if @user && @user.valid_password?(params[:password])
                    @user.api_key =  JWT.encode({id: @user.id, exp: 1.month.from_now.to_i}, Rails.application.secrets.secret_key_base)
                    @user.save!
                    render json: {user: @user, token: @user.api_key}
                else
                    render json: {error: "Invalid email or password"}
                end
            end
        
            def auto_login
                render json: @user
            end
            

        
            private
        
            def user_params
                params.permit(:email, :password, :name, :age, :ocupation, :phone, :api_key, :balance)
            end
        end
    end
end