class AddMovementToRegistrations < ActiveRecord::Migration[6.1]
  def change
    add_reference :registrations, :movement, null: false, foreign_key: true
  end
end
