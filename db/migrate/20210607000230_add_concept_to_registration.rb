class AddConceptToRegistration < ActiveRecord::Migration[6.1]
  def change
    add_reference :registrations, :concept, null: false, foreign_key: true
  end
end
