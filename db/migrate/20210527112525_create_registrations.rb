class CreateRegistrations < ActiveRecord::Migration[6.1]
  def change
    create_table :registrations do |t|
      t.integer :user_id
      #t.integer :type_movement_id
      t.decimal :amount
      #t.integer :type_concept_id
      t.date :date
      t.text :description

      t.timestamps
    end
  end
end
