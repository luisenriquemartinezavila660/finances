Rails.application.routes.draw do
  
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      # resources :registrations
      # resources :type_movements
      # resources :type_concepts

      resource :users, only: [:create]
      post "/login", to: "users#login"
      post "/register", to: "users#create"
      get "/auto_login", to: "users#auto_login"

      get 'types/concepts', to: 'concepts#index'
      post 'types/concepts', to: 'concepts#save_type_concept'
      put 'types/concepts/:id', to: 'concepts#update_type_concept'
      delete 'types/concepts/:id', to: 'concepts#delete_type_concept'

      get 'types/movements', to: 'movements#index'
      post 'types/movements', to: 'movements#save_type_movements'
      put 'types/movements/:id', to: 'movements#update_type_movement'
      delete 'types/movements/:id', to: 'movements#delete_type_movement'

      post 'registration', to: 'registrations#save_registration'
      get 'registration', to: 'registrations#index'
      put 'registration/balance', to: 'registrations#add_balance_user'
      get 'get_balance', to: 'registrations#get_balance'
    end
  end
end
